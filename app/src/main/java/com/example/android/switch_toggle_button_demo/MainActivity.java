package com.example.android.switch_toggle_button_demo;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ToggleButton mToggle=findViewById(R.id.togglebtn);
        final Switch mSwitch=findViewById(R.id.switchBtn);
        final View view=mToggle.getRootView();
        mToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                view.setBackgroundColor(Color.parseColor("#C5CAE9"));
                mSwitch.setChecked(true);
                }
                else{
                    view.setBackgroundColor(Color.WHITE);
                    mSwitch.setChecked(false);
                }

            }
        });
        mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    view.setBackgroundColor(Color.parseColor("#C5CAE9"));
                    mToggle.setChecked(true);
                }
                else{
                    view.setBackgroundColor(Color.WHITE);
                    mToggle.setChecked(false);
                }
            }
        });
    }
}
